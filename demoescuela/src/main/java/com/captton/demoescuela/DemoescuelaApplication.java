package com.captton.demoescuela;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoescuelaApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoescuelaApplication.class, args);
	}

}
