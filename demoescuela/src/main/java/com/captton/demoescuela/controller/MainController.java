package com.captton.demoescuela.controller;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.captton.demoescuela.model.model_escuela;


@RestController
@RequestMapping({"/escuela"})
public class MainController {

	@GetMapping("/bienvenida/{nombre}")
	public String bienvenida(@PathVariable String nombre) {
		return "Bienvenido a la página de nuestra escuela "+nombre;
	}
	
	@PostMapping({"/carga"})
	public ResponseEntity<Object> guardarDatos(@RequestBody model_escuela datos){
		
		System.out.println("Nombre y Apellido: "+datos.getNombre()+" "+datos.getApellido()+".");
		System.out.println("Direccion: "+datos.getDireccion()+".");
		System.out.println("Sexo: "+datos.getSexo()+".");
		System.out.println("Edad: "+datos.getEdad()+".");
		System.out.println("Telefono: "+datos.getTelefono()+".");
		
		
	JSONObject obj = new JSONObject();
	
		if(datos.getSexo().equals("F")) {
			
			try {
				obj.put("error", "0");
				obj.put("message", "Bienvenida a nuestra página: "+datos.getNombre()+" "+datos.getApellido());
				} catch (JSONException e) {}
			
		}else {
			try {
				obj.put("error", "0");
				obj.put("message", "Bienvenido a nuestra página: "+datos.getNombre()+" "+datos.getApellido());
				} catch (JSONException e) {}
		}
	
	
  return ResponseEntity.ok().body(obj.toString());

}
}
